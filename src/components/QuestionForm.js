import { useState } from 'react'


function QuestionForm() {
    const [inputValue, setInputValue] = useState('Posez votre question ici')
    return (
        <div>
            <textarea
                value={inputValue}
                onChange={(e) => checkValue(e.target.value,setInputValue)} // e.target.value accéder a la valeur de ce qui est rentré
            />
            <button onClick={() => alert(inputValue)}>Alertez moi 🚨</button>
        </div>
    )
}

function checkValue(value, test) {
    
    if (!value.includes('f')) {
        test(value)
    }
}

export default QuestionForm