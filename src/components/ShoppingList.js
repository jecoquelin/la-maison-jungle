import { useState } from 'react'
import { plantList } from '../datas/plantList'
import '../styles/ShoppingList.css'
import PlantItem from './PlantItem';
import Categories from './Categories';

function ShoppingList ({ cart, updateCart }){
	const [activeCategory, setActiveCategory] = useState('');

	let categories = [];
	plantList.forEach(elem => {
		if (!categories.includes(elem.category) ){
			categories.push(elem.category)
		}
	})

	function addToCart(name, price){
		const currentPlantAdded = cart.find((plant) => plant.name === name)
		if (currentPlantAdded){
			const cartFilteredCurrentPlant = cart.filter(
				(plant) => plant.name !== name
			)
			updateCart([
				...cartFilteredCurrentPlant,
				{ name, price, amount: currentPlantAdded.amount +1}
			])
		} else {
			updateCart([...cart, { name, price, amount:1 }])
		}
	}

	function removeToCart(name, price){
		const currentPlantAdded = cart.find((plant) => plant.name === name)
		if (currentPlantAdded){
			const cartFilteredCurrentPlant = cart.filter(
				(plant) => plant.name !== name
			)
			if (currentPlantAdded.amount > 0){
				updateCart([
				...cartFilteredCurrentPlant,
				{ name, price, amount: currentPlantAdded.amount - 1}
			])
			}
		} 
	}
	

	return (
		<div className='lmj-shopping-list'>

			<Categories
				categories = {categories}
				setActiveCategory = {setActiveCategory}
				activeCategory = {activeCategory}
			/>

			<ul className='lmj-plant-list'>
				{plantList.map(({ id, cover, name, water, light, price, category}) => !activeCategory || activeCategory === category ? (
					<div key = {id}>
						<PlantItem 
								name = {name} 
								cover = {cover} 
								id = {id} 
								water = {water} 
								light = {light}
								price = {price}
						/> 
						<button onClick={ () => addToCart( name, price)}>Ajouter</button>
						{ <button onClick={ () => removeToCart( name, price)}>Retire du Panier</button> }
					</div>
				): null 
				)}
			</ul>
		</div>
	)
}

export default ShoppingList