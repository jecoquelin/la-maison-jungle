import '../styles/Cart.css'
import { useState, useEffect } from 'react'

function Cart({ cart, updateCart }) {
    const [isOpen, setIsOpen] = useState(true);
    const item = Object.key(cart)
    const total = cart.reduce(
        (acc, planType) => acc + planType.amount * planType.price,
        0
    );
    if (total < 0){
        total = 0
    }

    useEffect(() => {
        document.title = `LMJ: ${total}€ d'achats`
    },[total])
    
    return isOpen ? (
        <div className="lmj-cart">
            <button className='lmj-cart-toggle-button' onClick={() => setIsOpen(false)}>Fermer</button>
            {cart.length > 0 ? (
                <div>
                    <h2>Panier</h2>
                    <ul>
                    {cart.map(({name, price, amount}, index) => (
                        <div key={`${name} - ${index}`}>
                            <li>{name} {price}€ x {amount}</li> 
                        </div>
                    ))}
                    </ul>
                    <h3>Total : {total}€</h3>
                    <button onClick={() => updateCart([])}>Vider le Panier</button>
                </div>
            ):(
                <div>Votre Panier est vide</div>
            )}
        </div>
        ):(
            <div className='lmj-cart-closed'>
                <button className='lmj-cart-toggle-button' onClick = {() => setIsOpen(true)}>Ouvrir le Panier</button>
            </div>
        )
        
}

export default Cart