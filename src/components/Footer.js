import { useState } from 'react'
import '../styles/Footer.css'

function InvalideValue(value){
	const arrobase = '@'
	if (!value.includes(arrobase)){
		alert(`Attention, il n'y a pas d'${arrobase}, ceci n'est pas une adresse valide`)
	}
}

function Footer() {
	const [inputValue, setInputValue] = useState('Email')

	return (
		<footer className='lmj-footer'>
			<div className='lmj-footer-elem'>
				Pour les passionné·e·s de plantes 🌿🌱🌵
			</div>
			<div className='lmj-footer-elem'>Laissez-nous votre mail :
			&nbsp;
			<input 
				value={inputValue}
				onChange={(e) => setInputValue(e.target.value)}
				onBlur={() => InvalideValue(inputValue)}
			/>
			</div>
		</footer>
	)
}

export default Footer